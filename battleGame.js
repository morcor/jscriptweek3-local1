(function() {
// 1. Create attack function below.  This will take the following parameters:
// attackingPlayer, defendingPlayer, baseDamage, variableDamage

// function attack(attackingPlayer, defendingPlayer, baseDamage, variableDamage);




// 2. Create player1 and player2 objects below
// Each should have a name property of your choosing, and health property equal to 10

const player1 = {
  name: 'Serrik',
  health: 10
};

const player2 = {
  name: 'Arlock',
  health: 10
};



// 3. Refactor attack function to an arrow function.  Comment out function above.

const attack = (player1, player2) => {
  return player1 + player2;

}
let attackingPlayer = player1;
let defendingPlayer = player2;


// DO NOT MODIFY THE CODE BELOW THIS LINE
// Set attacker and defender.  Reverse roles each iteration
let attackOrder = [player1, player2];

// Everything related to preventInfiniteLoop would not generally be necessary, just adding to
// safeguard students from accidentally creating an infinite loop & crashing browser
let preventInfiniteLoop = 100;
while (player1.health >= 1 && player2.health >= 1 && preventInfiniteLoop > 0) {
  const [attackingPlayer, defendingPlayer] = attackOrder;
  console.log(attack(attackingPlayer, defendingPlayer, 1, 2));
  attackOrder = attackOrder.reverse();

  preventInfiniteLoop--;
}
const eliminatedPlayer = player1.health <= 0 ? player1 : player2;
console.log(`${eliminatedPlayer.name} has been eliminated!`);



// function attack(Player1,player2,baseDamage,variableDamage) {
//   //calculate total damage
//   const TotalDamage = baseDamage + Math.floor(Math.random() * (variableDamage + 1))
//   let stringResult = `${Player1.name} hit ${player2.name} for ${TotalDamage}.`
//   //reduce the health property
//   if (player2.health - TotalDamage < 0) {
//       player2.health = 0
//       stringResult = stringResult + `And ${player2.name} died!`
//   } else {
//       player2.health = player2.health - TotalDamage
//      stringResult = `${attackingPlayer.name} hit ${player2.name} for ${TotalDamage}.`
//  }
//  return stringResult
// }
})();