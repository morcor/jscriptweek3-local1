(function() {

// create function logReceipt that accepts menu items (1 or many) as objects
// with these properties: {descr, price}
// i.e. {descr: 'Coke', price: 1.99}
// function should log each item to the console and log a total price

const burrito = { descr: 'Lengua Burrito', price: 7.99 };
 const sides = { descr: 'Chips & Salsa', price: 2.99 };
   const drink = { descr: 'Dr. Pepper', price: 1.99 };

 const logReceipt = (...groceries) => {
     let total = 0;
     groceries.forEach(grocery => {
         console.log(`${grocery.descr} - $${grocery.price}`);
         total += grocery.price;
     });
     console.log("Total - $" + total);
 };

 logReceipt(burrito, sides, drink);


// Check
// logReceipt(
//   { descr: 'Lengua Burrito', price: 5.99 },
//   { descr: 'Chips & Salsa', price: 2.99 },
//   { descr: 'Sprite', price: 1.99 }
// );
// should log something like:
// Burrito - $5.99
// Chips & Salsa - $2.99
// Sprite - $1.99
// Total - $10.97
})();