 (function() {

  const RESULT_VALUES = {
    w: 3,
    d: 1,
    l: 0,
  
      getTotalPoints:function(){
      let tempTotal = 0;
        
        const argumentList = Array.from(arguments);
          argumentList.forEach(function (stringInput){
        
          for (let i = 0; i < stringInput.length; i++) {

          tempTotal += ( RESULT_VALUES[stringInput.charAt(i)] );
        }
      })
       return tempTotal;
      }
    };
    
  console.log(RESULT_VALUES.getTotalPoints('wwdl'));

  
  // console.log(getTotalPoints('wwdl'));

/**
 * Takes a single result string and (one of 'w', 'l', or 'd') 
 * and returns the point value
 * 
 * @param {string} result 
 * @returns {number} point value
 */


// Create getTotalPoints function which accepts a string of results
// including wins, draws, and losses i.e. 'wwdlw'
// Returns total number of points won

// string is property strings and each letter is property name.


// Check getTotalPoints
// console.log(getTotalPoints('wwdl')); // should equal 7

// create orderTeams function that accepts as many team objects as desired, 
// each argument is a team object in the format { name, results }
// i.e. {name: 'Sounders', results: 'wwlwdd'}
// Logs each entry to the console as "Team name: points"

// const RESULT_VALUES = {

//               
const orderTeams = (...teamList) => {
  teamList.forEach(team => {
  // console.log(team.name + ': ' + RESULT_VALUES.getTotalPoints(team.results));
  console.log(`${team.name}: ${RESULT_VALUES.getTotalPoints(team.results)}`);
  
})
};



// Check orderTeams
orderTeams(
  { name: 'Sounders', results: 'wwdl' },
  { name: 'Galaxy', results: 'wlld' }
);
// should log the following to the console:
// Sounders: 7
// Galaxy: 4
})();