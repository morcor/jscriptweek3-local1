// 1. Create a class function SpaceShip
// - should set two properties: name and topSpeed
// - should have a method accelerate that logs to the console 
//   `${name} moving to ${topSpeed}`
(function() {

class Spaceship {
    constructor(name, ability) {
        this.name = name;
        this.acceleratingTo = ability;

    }
    useAbility() {
        const { name, ability } = this;
        console.log(`${name} uses ${ability}`);
    }
}


// 2. Call the constructor with a couple ships, 
// and call accelerate on both of them.

const enterprise = new Spaceship("Enterprise", "Warp 8");
enterprise.ability;

const redDwarf = new Spaceship("Red Dwarf", "Full Thrust!");
redDwarf.ability;

console.log(enterprise);
console.log(redDwarf);

})();
